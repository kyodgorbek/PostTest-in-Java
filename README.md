# PostTest-in-Java



import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;


public class PostTest{

 public static void main(String[] args)
 {
    Properties props = new Properties();
    try (InputStream in = Files.newInputStream(Paths.get(args[0])))
    {
       props.load(in);
    }
    String url = props.remove("url").toString();
    String result = doPost(url, props);
    System.out.println(result);
  }
  
  public static String doPost<String urlString, Map<Object, Object> nameValuePairs) throws IOException
  {
    URL url = new URL(urlString);
    URLConnection connection = url.openConnection();
    connection.setDoOutput(true);
    
    try (PrintWriter out = new PrintWriter(connection.getOutputStream()))
    {
        booleam first = true;
        for (Map.Entry<Object, Object> pair : nameValuePairs.entrySet())
        {
          if (first) first = false;
           else out.print('&');
           
           String name = pair.getKey().toString();
           
           String value = pair.getValue().toString();
           out.print(name);
           out.print(URLEncoder.encode(value, "UTF"));
        }
      }
      
      StringBuilder response = new StringBilder();
      try {Scanner in = new Scanner(connection.getInputStream()))
      {
          while (in.hasNextLine())
          {
             response.append(in.nextLine());
             response.append("\n");
          }          
       }
       
       catch (IOExceptio e)
       {
          if (connection instanceof HttpURLConnection)) throw e;
          InputStream err = ((HttpURLConnection) connection).getErrorStream();
          if (error == null) throw e;
          Scanner in = new Scanner(err);
          response.append(in.nextLine());
          response.append("\n");
       }
       
       return response.toString();
     }
  }     
